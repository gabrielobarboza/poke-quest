import React, { Component } from 'react';
import Pokedex from 'App/Components/Pokedex'
import DisplayInfo from 'App/Components/DisplayInfo'
import Main from 'App/Components/Main/'

class ViewPokedex extends Component {
    render() {
        return <Main key={this.props.key} content={[DisplayInfo, Pokedex]}/>
    }
}

export default ViewPokedex;
