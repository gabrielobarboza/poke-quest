const BACKOFFICE = 'http://localhost:3003/api'
const API = {
	INGREDIENTS: `${BACKOFFICE}/ingredients`,
	POKEMON: `${BACKOFFICE}/pokemon`,
	SKILLS: `${BACKOFFICE}/skills`,
	POTS: `${BACKOFFICE}/pots`,	
}


module.exports = {BACKOFFICE, API}