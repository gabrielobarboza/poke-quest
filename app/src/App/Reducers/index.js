import screenReducer from './Screen/screenReducer';
import skillsReducer from './Skills/skillsReducer';
import dexReducer from './Dex/dexReducer';
import potReducer from './Pot/potReducer';

import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    screen : screenReducer,
    skills: skillsReducer,
    dex : dexReducer,
    pot : potReducer
})

export default rootReducer