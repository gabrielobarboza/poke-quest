
const ingredients = {tm: 0, a: 0, f: 0, bb: 0, br: 0, ir: 0, bm: 0, h: 0, rm: 0, ms: 0}

const POT_INITIAL_STATE = { active: null, list: [], recipe: [], selected: null, ingredients, data: []}

const parseIngredients = (current) => {
	let parseIngredients = { ...ingredients }
	
	current.recipe.forEach((ing, i) => parseIngredients[ing] += 1)
	current.ingredients = parseIngredients
	
	return { ...current };
}
 
export default (state = POT_INITIAL_STATE, action) => {
	switch(action.type) {

		case 'SET_DATA_POTS':
		let { data } = action
		
		if(Array.isArray(data)) {
			state.list = data
		} 

		return {...state}

		case 'SET_DATA_INGREDIENTS':
			state.data = action.data
			return {...state}
		break;

		case 'SET_POT_TYPE':
			state.active = action.active
			return {...state}
		break;

		case 'REMOVE_INGREDIENT':
			if(!state.selected){
				state.recipe[parseInt(action.position)] = ''
			}

			return { ...parseIngredients(state)}
		break;
		
		case 'SELECT_INGREDIENT':
			state.selected = action.slug
			return {...state}
		break;

		case 'ADD_INGREDIENT':
			if(state.selected){
				if(action.dbclick) {
					if(state.recipe.length) {
						let empty = state.recipe.indexOf("");

						if(empty !== -1) {
							state.recipe[empty] = state.selected
						} else if (state.recipe.length < 5){
							state.recipe.push(state.selected)
						} else {
							state.recipe[0] = state.selected
						}
					} else {
						state.recipe.push(state.selected)
					}
				} else {
					state.recipe[parseInt(action.position)] = state.selected
				}
				state.selected = null
			}
			return {...parseIngredients(state)}
		break;

        default:
			return state
    }
}