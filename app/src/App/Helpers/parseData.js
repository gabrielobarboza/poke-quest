const parseSubData = (obj, data, sub) => {
    let parse = ''

    if(obj) {
        parse = obj

        if(data) {
            parse = obj[data]

            if(sub) parse = obj[data][sub]
        }
    } 

    return parse
}

export {
    parseSubData
}