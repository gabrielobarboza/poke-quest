const toCaptalize = string => string ? (string[0].toUpperCase() + string.slice(1)).replace(/(\w*) ([a-zA-Z]{1})(\w*)/, (match, $1, $2, $3) => $1+" "+$2.toUpperCase()+$3) : "";
const rmDash = string => string.replace(/(\w*)\-([a-zA-Z]{1})(\w*)/, (match, $1, $2, $3) => $1+" "+$2.toUpperCase()+$3)

export {
    rmDash,
    toCaptalize
}