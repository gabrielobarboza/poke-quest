import axios from 'axios'

const getData = (config, cb) => {
	axios(config).then(res => {
		cb(res.data)
	});
} 
export default getData