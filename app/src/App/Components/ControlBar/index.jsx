import React, { Component } from 'react';

class ControlBar extends Component {
    render() {
        let {className, id, children} = this.props
        return (
            <div className={`control_bar ${className}`} id={id}>
                {children}
            </div>
        );
    }
}

export default ControlBar;
