import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import cls from "classnames";

import { toCaptalize, rmDash } from 'App/Helpers/String';
import { parseSubData } from 'App/Helpers/parseData';
import { selectMonster } from 'App/Reducers/Dex/dexActions';

class Info extends Component {
    constructor(props){
        super(props)

        this.state = {
            dexID: this.props.dex.selected,
            pokemon: null
        }
    }

    componentDidMount() {
        this.props.selectMonster(1)
    }

    render() {
        let { pokemon } = this.props.dex
        let rangeType = pokemon ? (pokemon.gameData.meleePercent == 100 ? "melee" : "ranged") : ""
        let pokeName = pokemon ? rmDash(toCaptalize(pokemon.name)) : "";

        return (
            <div id="Info" className={cls({'hidden': !pokemon })}>
                <div className="header-info">
                    <div className="info-image">
                        <img src={`/assets/images/pokemonicons/icon_m${this.props.dex.selected}.png`} alt={pokeName}/>
                    </div>
                    <div className="info-content">
                        <div className="info-name">{pokeName}</div>
                        <div className={`info-range ${rangeType}`} />
                        <div className="info-base_stats">
                            <div className="stat_value hp-stat">{parseSubData(pokemon ,'gameData', 'hpBasis') }</div>
                            <div className="stat_value atk-stat">{parseSubData(pokemon ,'gameData', 'attackBasis') }</div>
                        </div>
                    </div>
                    <div className="info-types">
                        {pokemon ? pokemon.types.map((type, i) => <div key={i} className={`type-flag type-${type.toLowerCase()}`}><span>{type}</span></div>) : null} 
                    </div>
                </div>
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ selectMonster }, dispatch)
export default connect(StateToProps, DispatchToProps)(Info)
