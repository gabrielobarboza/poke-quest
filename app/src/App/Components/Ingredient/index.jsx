import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import { selectIngredient } from 'App/Reducers/Pot/potActions';

import { DragSource } from 'react-dnd';

import cls from "classnames";

class Ingredient extends Component {

	constructor(props) {
        super(props);

        this.renderIngredient = this.renderIngredient.bind(this);
	}

	componentDidMount(){
        this.setBackground();
        const preview = new Image();
        preview.src = `assets/images/ingredients/${this.props.img}`
        preview.style.transform = "scale(.5)"
        preview.onload = () => this.props.connectDragPreview(preview);
    }

	setBackground(){
        this.setState({ mounted: true }, () => {
            this.ingredientImage.style.backgroundImage = `url(/assets/images/ingredients/${this.props.img}`;
            this.ingredientImage.style.backgroundSize = 'cover'
            this.ingredientImage.style.opacity = 1
        })
    }
	
    renderIngredient(){
        let { connectDragSource, isDragging , pot, slug } = this.props
        return connectDragSource(
            <button onClick={this.props.onClick} className={cls("ingredient_default", {selected: pot.selected === slug})} ref={btn => this.btn = btn}>
                <div className="ingredientImage" ref={ image => this.ingredientImage = image }></div>
                <div className="ingredientName">{this.props.name}</div>
            </button>
        );
    }

    render() {
        return this.renderIngredient()
    }
}

const ingredientSource = {
    beginDrag(props) {
        props.selectIngredient(props.slug)
        return {}
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    }
}

Ingredient.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
};

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ selectIngredient }, dispatch)
export default connect(StateToProps, DispatchToProps)(DragSource("Ingredient", ingredientSource, collect)(Ingredient))
