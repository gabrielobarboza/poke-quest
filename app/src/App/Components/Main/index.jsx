import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { setScreen } from 'App/Reducers/Screen/screenActions';

import Overlay from 'App/Components/Overlay'
import ContentWrapper from 'App/Components/Wrapper'

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mounted: false
        }

        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentWillMount() {
        // console.log(this.props.screen)
        window.addEventListener("resize", this.updateDimensions, false);        
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions, false);
    }

    componentDidMount() {
        this.setState({ mounted: true})
    }

    updateDimensions () {
        this.props.setScreen({
            landscape: window.innerWidth >= window.innerHeight,
            width: window.innerWidth,
            height: window.innerHeight
        })
        this.forceUpdate()
    }

    shouldComponentUpdate(props, state) {
        return props.screen !== this.props.screen || !this.state.mounted
    }
    
    render() {
        return (
            <div id="Main" className="main page-enter-done" >
                <Overlay />
                <ContentWrapper content={this.props.content}/>
            </div>
        )
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ setScreen }, dispatch)
export default connect(StateToProps, DispatchToProps)(Main)

