import React, { Component } from 'react';

class ContentWrapper extends Component {
	constructor (props){
		super(props);

		this.state = {
			rows: []
		}

		this.renderRows = this.renderRows.bind(this)
	}
	
	componentDidMount(){
		let columns = 3
		let rows = []

		this.props.content.map((child, i) => {
			let row = parseInt(i / columns)
			if(rows[row]){
				rows[row].content.push(child)
			} else {
				rows.push({
					content: [child]
				})
			}
		})
		
		this.setState({
			rows
		})
	}

	renderRows() {
		return this.state.rows.map((row, key) => {
			return(
				<div className="app-content-wrapper" key = {key}>
					{this.renderColumns(row.content)}
				</div>
			)
		})
	}

	renderColumns(content) {
		return content.map((Child, key) => {
			return (
				<div className="column-container" key = {key}>
					<Child />
				</div>
			)
		})
	}

	render() {
		let Wrapper = () => <div id="Wrapper" className="app-wrapper">{this.renderRows()}</div>
		return <Wrapper/>;
	}
}

export default ContentWrapper;
