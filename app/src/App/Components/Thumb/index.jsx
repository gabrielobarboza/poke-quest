import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { selectMonster } from 'App/Reducers/Dex/dexActions';

class Thumb extends Component {

    constructor(props) {
        super(props);

        this.renderThumb = this.renderThumb.bind(this);
        this.changeMonster = this.changeMonster.bind(this);
    }

    componentDidMount(){
        this.setBackground();
    }

    setBackground(){
        this.setState({ mounted: true }, () => {
            this.mosterImage.style.backgroundImage = `url(/assets/images/pokemonicons/icon_m${this.props.dexid}.png)`;
            this.mosterImage.style.backgroundSize = 'cover'
            this.mosterImage.style.opacity = 1    
        })
    }

    changeMonster() {
        this.props.selectMonster(this.props.dexid)
    }

    renderThumb(){
        let {className, number, name} = this.props
        return (
            <button className="thumb_default" onClick = {this.changeMonster}>
                <div className="numberTab">
                    <div className="numberDigits">{('00' + this.props.dexid).slice(-3)}</div>
                </div>
                <div className="monsterImage" ref={ image => this.mosterImage = image }></div>
                <div className="monsterName">{this.props.name || "Pokemon"}</div>
            </button>
        );
    }

    render() {
        return this.renderThumb()
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ selectMonster }, dispatch)
export default connect(StateToProps, DispatchToProps)(Thumb)
