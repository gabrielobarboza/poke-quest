import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';

import DATA from 'App/Data/appData'
import getData from 'App/Services/getData';

import { addIngredient, removeIngredient } from 'App/Reducers/Pot/potActions';

class PotSlot extends Component {

	constructor(props) {
		super(props);

		this.state = {
			ingredient: null
		}
		
		this.manageIngredients = this.manageIngredients.bind(this);
		this.renderPotSlots = this.renderPotSlots.bind(this)
	}

	componentWillMount(){
	}
	
	componentWillReceiveProps(props) {
		let ingredient = props.pot.data.filter(ing => ing.slug == props.ingredient)[0]
		let change = false
		this.setState((prevState) =>{
			change = prevState.ingredient !== ingredient
			return change ? { ingredient: null } : { ...prevState }
		}, () => {
			if(change) this.setState({ ingredient })
		})
	}

	manageIngredients(){
		if(this.props.ingredient) this.props.removeIngredient(this.props.slotID)
		this.props.addIngredient(this.props.slotID)
	}

	getQuantity(active){
		let obj = this.props.pot.list.filter(pot => pot.name === active)[0]
		return obj.requires
	}

	renderPotSlots() {

		let { ingredient } = this.state
		let { connectDropTarget, isOver } = this.props
		let style = {
			backgroundImage : ingredient ? `url(/assets/images/ingredients/${ingredient.img})` : null,
		}
		return connectDropTarget(
			<div title={ingredient ? ingredient.name : null} 
			className={"pot_slot" + (ingredient ? ` item-${ingredient.slug} filled` : '') + (isOver ? " hover" : "")} 
			onClick={this.manageIngredients} style={style}>
				<div className={"pot_amount" + (ingredient ? "_active" : null)}>{ingredient ? this.getQuantity(this.props.pot.active) : ""}</div>
			</div>
		);
	}
	
	render() {
		return this.renderPotSlots();
	}
}

const manageIngredients = props => {
	if(props.ingredient) props.removeIngredient(props.slotID)
	props.addIngredient(props.slotID)
}

const slotTarget = {
	
	drop(props) {
		manageIngredients(props)
	}
};

function collect(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget(),
		isOver: monitor.isOver()
	};
}

PotSlot.propTypes = {
	connectDropTarget: PropTypes.func.isRequired,
	isOver: PropTypes.bool.isRequired
};

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ addIngredient, removeIngredient }, dispatch)
export default connect(StateToProps, DispatchToProps)(DropTarget("Ingredient", slotTarget, collect)(PotSlot))

