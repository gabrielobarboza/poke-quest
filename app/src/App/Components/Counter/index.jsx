import React, { Component } from 'react';
import { connect } from 'react-redux';
import cls from 'classnames'

class Counter extends Component {

	constructor(props) {
        super(props);

        this.state = {
		}            
		
        this.calcTotal = this.calcTotal.bind(this);
    }

	calcTotal(quantity){
		let { active } = this.props.pot
		let multiplier = this.props.pot.list.filter(pot => pot.name === active)[0]

		multiplier ? multiplier = multiplier.requires : 0

		return quantity ? quantity * multiplier : 0
	}

	render() {
		let items = []
		return (
			<div id="Counter">
				<ul>
					{console.log(this.props.pot)}
					{this.props.pot.data.map(item => {
						return(<li className={cls({active : this.props.pot.ingredients[item.slug] > 0})} key={item.slug}>
							<img src={`assets/images/ingredients/${item.img}`} className={"icon_ingredient"} />
							<p>{this.calcTotal(this.props.pot.ingredients[item.slug])}</p>
						</li>)
					})}
				</ul>
			</div>
		);
	}
}
const StateToProps = state => ({ ...state })
export default connect(StateToProps)(Counter)
