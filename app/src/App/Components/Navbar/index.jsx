import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import RoutesList from "App/data/Routes.js"

import cls from "classnames"

class Navbar extends Component {
    renderLink(route) {
        let isActive = this.props.route.pathname === route.path
        return(<li key={route.key} className={cls({'active':isActive})}> {isActive ? (
            <span className="nav-item_link">{route.label}</span>
         ) : (
            <NavLink exact={route.exact} className="nav-item_link" to={route.path} >{route.label}</NavLink>
        )} </li>)
        
    }

    render() {
        return (
        <nav id="Navbar" className="nav-bar">
            <ul>
                {RoutesList.map((route, i) =>
                    route.label ? this.renderLink(route) : null
                )}
            </ul>
        </nav>
        );
    }
}

export default Navbar;
