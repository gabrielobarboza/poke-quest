
const apiData = require('../apiData.json')
const fs = require('fs')
const jsonfile = require('jsonfile')

let path = apiData.path.game
let file_name = 'skilldependdataset'

const gameSkillDependDataToJSON = (db, cb) => {

    let target = `${apiData.path.data}/json/converted/${file_name}.json`

    fs.stat(target, (err, stat) => { 
        if (err && err.code == 'ENOENT') {

            let sizes = /\t.*size = \d.*\n/gm
            let types = /(int|UInt8|SInt64|string|Array\s?|UInt16|vector|float|PPtr<MonoScript>|PPtr<GameObject>)\s/gm
            let positions = /\t.*\[\d.*\]\n/gm
            let pkmDataList = /(\t.*SkillDependData data)/gm
            let effects = /\seffects\n(\s*data = \d*\s*)*?\s*(conditions)/gm
            let condition = /\s*data = (\d.*)/gm
            let closeArr = /(\[)\s*(\{)/gm
            let toParseJson = /,(\s*{)/gm
            let endJson = /("conditions":\s*\[\s*$)/g
            let fixInitJson = /^\[\]},{/g
                
            
            fs.readFile(`${path}/${file_name}.txt`, 'utf8', (err, data) => {
                if (err) throw err;
                
                let n = 0
                
                data = data.substring(data.indexOf('m_datas') + 'm_datas'.length);
                data = data.replace(/m_/gm, '');
                data = data.replace(/^/, "[\n")
                
                data = data.replace(sizes, '');
                data = data.replace(types, '');
                data = data.replace(positions, '');
                data = data.replace(pkmDataList, match => {
                    match = match.replace(pkmDataList, `\t\t{\n"id":${n},`)
                    n ++
                    return match
                })
                data = data.replace(effects, `"$2": [`)
                data = data.replace(condition, `$1,`)
                data = data.replace(toParseJson, `]},$1`)
                data = data.replace(closeArr, `$1]},$2`)
                data = data.replace(endJson, `$1]}]`)
                data = data.replace(fixInitJson, `[{`)
                
                console.log(data);
                let result = JSON.parse(data)

                jsonfile.writeFile(target, result, {spaces:4, EOL:"\r\n"}, error => {
                    if(error) console.log(error)
                    return cb && cb(null, db)
                })
            })
        } else {
            return cb && cb(null, db)
        }
    });
}

module.exports = gameSkillDependDataToJSON