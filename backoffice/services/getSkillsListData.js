const async = require("async");
const curl = require("curl");
const jsdom = require("jsdom");
const jsonfile = require('jsonfile')

const apiData = require('../apiData.json')
const url = "https://www.serebii.net/quest/attacks.shtml";

let parsedData = []

function parseData(html){
    const {JSDOM} = jsdom;
    const dom = new JSDOM(html);
    const $ = (require('jquery'))(dom.window);

    const table = $(".dextable");
    const lines = table.find('tr').not(':nth-child(1)')

    async.forEach(lines, (line , cb) => {
        const columns = $(line).find('td')
        
        const name  = columns.eq(1).text().trim().toLowerCase();
        const type = columns.eq(2).find('img').attr('src').replace(/\/pokedex-bw\/type\/([a-z].*).gif$/, (str, type) => type);
        const description = columns.eq(5).text().trim();
        const moveData = { name, type, description }
        
        parsedData.push(moveData)

        cb (null)
    }, err => {

        jsonfile.writeFile(`${apiData.path.data}/json/skillsListData.json`, parsedData, {spaces: 4, EOL: '\r\n'}, (err) => {
            if (err) console.error(err)        
        })
    })
}

function getSkillsListData() {
   curl.get(url, null, (err,resp,body)=>{
        if(resp.statusCode == 200){
            return parseData(body);
        } else{
            console.log("error while fetching url");
        }
    });
}

getSkillsListData()

module.exports = getSkillsListData