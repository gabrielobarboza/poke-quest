const fs = require('fs')
const async = require('async')
const convert = require('fbx2gltf')

const fbx = '../app/assets/meshes/fbx/'
const gltf = '../app/assets/meshes/gltf/'

fs.readdir(fbx, function(err, models) {
    async.forEach(models, (model, cb) => {
        model.replace('.fbx', '.glb')

        convert(fbx + model, gltf + model.replace('.fbx', '.glb')).then(
            destPath => {
                console.log("New model saved as: " + destPath)
            },
            error => {
                console.log(error)
            }
        )
    })
}, err => { console.log(err)})