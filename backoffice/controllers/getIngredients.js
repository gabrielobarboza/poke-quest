const database = require('../config/database');
const getIngredientsList = require("../api/getIngredientsList")

const getIngredients = (req, res, next) => {
    
    database.connect((err, client) => {
        
        getIngredientsList(client, list => res.json(list))
    })
}

module.exports = getIngredients