const database = require('../config/database');
const getPotsList = require("../api/getPotsList")

const getPots = (req, res, next) => {
    
    database.connect((err, client) => {
        
        getPotsList(client, list => res.json(list))
    })
}

module.exports = getPots