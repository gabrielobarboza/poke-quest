
const potsList = require('../data/Pots')

const dbPublishList = require('./dbPublishList')

const setPotsList = (db, cb) => {
    // console.log("run: setPotsList")
	let collection = db.collection("potsList")

	let list = potsList

	let params = {
		db,
		list,
		collection, 
		prop: 'name',
		name: 'POTS LIST'
	}

	dbPublishList(params, cb)
}

module.exports = setPotsList