const async = require('async')
const dbPublishList = require('./dbPublishList')
const getPokemonSkills = require('./getPokemonSkills')

const pokemonList = require('../data/Pokemons').gen1.pokemon_species

let pokemonGameData = require('../data/json/converted/pokemondataset.json')

const setPokemonList = (db, cb) => {
	
	// console.log("run: n: setPokemonList")

	let collection = db.collection("pokemonList")

	async.map(pokemonList, (obj, _cb) => {
		let { name, url } = obj
		let dexID = Number(url.match(/\/(\d.*)\//)[1])

		let gameData = pokemonGameData.filter(pokemon => {
			const match = Number(pokemon.monsterNo) === dexID
			return match
		})[0]

		async.filter(gameData.skillIDs, (id, __cb) => {
			getPokemonSkills(db, [id], skills => {				
				__cb(null, skills.length)
			})
		}, (err, skillIDs) => {
			if(err) console.log(err)

			gameData = { ...gameData, skillIDs }

			_cb(null,{
				name,
				dexID,
				gameData
			})
		})
	}, (err, list) => {
		if(err) console.log(err)

		let params = {
			db,
			list,
			collection, 
			prop: 'dexID',
			name: 'POKEMON LIST'
		}
	
		dbPublishList(params, cb)
	})
}

module.exports = setPokemonList