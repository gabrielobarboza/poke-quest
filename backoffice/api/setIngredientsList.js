const ingredientsList = require('../data/Ingredients')

const dbPublishList = require('./dbPublishList')

const setIngredientsList = (db, cb) => {
    // console.log("run: setIngredientsList")
	let collection = db.collection("ingredientsList")

	let list = ingredientsList.map((obj, id) => {
		obj.id = id
		return obj
	})

	let params = {
		db,
		list,
		collection, 
		prop: 'id',
		name: 'INGREDIENT LIST'
	}

	dbPublishList(params, cb)
}

module.exports = setIngredientsList