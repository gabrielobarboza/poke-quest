const async = require("async")

const dbPublishList = (params, cb) => {
	let { name, list, prop, collection, db} = params

    if(!name || !list || !prop || !collection) {
        console.log(`ERROR: dbPublishList required params:`)
        if(!collection) console.log(`"collection" => [Object]: The Collection Object at Database.`)
        if(!name) console.log(`"name" => [String]: A markup name for the list.`)
        if(!list) console.log(`"list" => [Array]: An Array list of Objects to publish on Database.`)
        if(!prop) console.log(`"prop" => [String]: The property to verify if Object of list exists as a document on Database.`)
        return
    }

    async.filter(list, (obj, callback) => {
        let query = {}
		query[prop] = obj[prop]
		
		collection.find(query).toArray((err, docs) => {
			callback(err, !docs.length)
		})
	}, (error, results) => {
        if (error) console.log(error)

		if (results.length) {
			collection.insertMany(results, err => {
				if(err) console.log(err)
				console.log(`${name.toUpperCase()} PUBLISHED!`)
				return cb && cb(null, db)
			})
		} else {
			return cb && cb(null, db)
		}
	})
}

module.exports = dbPublishList