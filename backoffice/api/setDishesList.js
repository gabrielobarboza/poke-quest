const async = require("async")
const dishesList = require('../data/Dishes')
const dbPublishList = require('./dbPublishList')

function objPokemon(arr, cb) {
	async.map(arr, (obj, callback ) => {
		let { name, quality, rate } = obj
		name = name.toLowerCase()
		
		let quality_rate = {}
		quality.forEach((q, i) => {
			quality_rate[q] = rate[i]			
		})

		const pokemon = {
			name,
			rate: quality_rate
		}

		callback(null, pokemon)
	}, (err, results) => {
		if(err) console.log(err)
		return cb && cb(results) || results || []
    })
}

function objVariations(arr, cb) {
	async.map(arr, (obj, callback ) => {
		let { quality } = obj

		objIngredients(obj.ingredients, ingredients => {
			const variation = {
				ingredients,
				quality
			}

			callback(null, variation)
		})

	}, (err, results) => {
		if(err) console.log(err)
		return cb && cb(results) || results || []
    })
}

function objIngredients(arr, cb) {
	const ingredients = {
		tm: 0,
		a: 0,
		f: 0,
		bb: 0,
		br: 0,
		ir: 0,
		bm: 0,
		h: 0,
		rm: 0,
		ms: 0,
		arr
	}

	async.each(arr, (ing, callback) => {
		//o gabriel não quer que...
		ingredients[ing] += 1
		callback(null)
	}, err => {
		if(err) console.log(err)
		return cb && cb(ingredients) || ingredients || {}
	})

}

function objDishes(arr, cb) {
	async.map(arr, (obj, callback) => {
        let { dish_name , type_id, type_name, variations , pokemon } = obj
		// console.log(type_name, variations.length)

		const dishe = {
			id: type_id,
			name: dish_name,
			type: type_name,
			variations: [],
			pokemon : []
		}

		objVariations(variations, vars => {
			dishe.variations = vars

			objPokemon(pokemon, pkm => {
				dishe.pokemon = pkm

				callback(null, dishe)
			})
		})

    }, (err, results) => {
		if(err) console.log(err)
		return cb && cb(results) || results || []
    })
}

const setDishesList = (db, cb) => {
	// console.log("run: n: setDishesList")
	
	let collection = db.collection("dishesList")

    objDishes(dishesList, list => {		
		let params = {
			db,
			collection, 
			list,
			prop: 'id',
			name: 'DISHES LIST'
		}
		
		dbPublishList(params, cb)
		return
    })

}

module.exports = setDishesList
