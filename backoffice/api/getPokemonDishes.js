const async = require('async')
const database = require('../config/database');

const getPokemonDishes = (client, data, cb) => {
	let db = client.db(database.name)
	let { dishName, pokemonName, dishID, dishType } = data

	let collection = db.collection("dishesList")
	
	let query = {}
	if(pokemonName) query = { "pokemon.name": pokemonName }
	if(dishName) query = { "name": dishName }
	if(dishID) query = { "id": dishID }
	if(dishType) query = { "type": dishType }

    collection.find(query).toArray((err, docs) => {
		if(err) console.log(err)
		
		async.map(docs, (dish, callback) => {
			let {id, name, type, pokemon, variations} = dish
			
			if(pokemonName) {
				pokemon = pokemon.filter(poke => poke.name.toLowerCase() === pokemonName)[0]
				variations = variations.filter(variation => !!pokemon.rate[variation.quality])
			} 
			
			callback(null, { id, name, type, pokemon, variations })
			
        }, (err, result) => {
			client.close()
            return cb && cb(result) || result || []
        })
    })
}
module.exports = getPokemonDishes