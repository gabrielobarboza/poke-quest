const Ingredients = [
    {
        name:"Tiny Mushroom",
        slug: "tm",
        img: "item_red_c.png"
    },
    {
        name:"Apricorn",
        slug: "a",
        img: "item_yellow_c.png"
    },
    {
        name:"Fossil",
        slug: "f",
        img: "item_grey_c.png"
    },
    {
        name:"Bluk Berry",
        slug: "bb",
        img: "item_blue_c.png"
    },
    {
        name:"Big Root",
        slug: "br",
        img: "item_red_uc.png"
    },
    {
        name:"Icy Rock",
        slug: "ir",
        img: "item_blue_uc.png"
    },
    {
        name:"Balm Mushroom",
        slug: "bm",
        img: "item_grey_uc.png"
    },
    {
        name:"Honey",
        slug: "h",
        img: "item_yellow_uc.png"
    },
    {
        name:"Rainbow Matter",
        slug: "rm",
        img: "item_rainbow_r.png"
    }, 
    {
        name:"Mystical Shells",
        slug: "ms",
        img: "item_mystic_r.png"
    }
]
module.exports = Ingredients
