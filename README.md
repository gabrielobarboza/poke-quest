# Pokémon Quest Database

## Requirements
- Node v8.5
- MongoDB Community Server 4.0.1

Structure
-------
Project has the following directories with different packages:

-  **./app**: Has the front-end node package files.
-  **./backoffice**: Has the back-end node package files.

Running the project
-------
- Install the packages in 'app' and 'backoffice' directories with `npm install`.
- Start mongod application.
- Access 'backoffice' folder and run `npm start`.
- Access 'app' folder and run `npm start`.
- Application will be available at localhost:9090.
