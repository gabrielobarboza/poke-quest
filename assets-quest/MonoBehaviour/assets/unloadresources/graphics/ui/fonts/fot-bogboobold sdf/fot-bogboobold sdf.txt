PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 1
	SInt64 m_PathID = 4002064078594445791
string m_Name = "FOT-BogbooBold SDF"
int hashCode = -773761780
PPtr<$Material> material
	int m_FileID = 0
	SInt64 m_PathID = -2880615089507529789
int materialHashCode = -1858938873
int fontAssetType = 1
FaceInfo m_fontInfo
	string Name = "FOTBogboo"
	float PointSize = 32
	float Scale = 1
	int CharacterCount = 44
	float LineHeight = 29.6875
	float Baseline = 0
	float Ascender = 21.6875
	float CapHeight = 0
	float Descender = -8
	float CenterLine = 0
	float SuperscriptOffset = 21.6875
	float SubscriptOffset = -3.2
	float SubSize = 0.5
	float Underline = -3.2
	float UnderlineThickness = 1.6
	float strikethrough = 4.977273
	float strikethroughThickness = 1.6
	float TabWidth = 96.25
	float Padding = 5
	float AtlasWidth = 256
	float AtlasHeight = 256
PPtr<$Texture2D> atlas
	int m_FileID = 0
	SInt64 m_PathID = 2525487457806468279
TMP_Glyph m_glyphInfoList
	Array Array
	int size = 44
		[0]
		TMP_Glyph data
			int id = 32
			float x = 6
			float y = 261
			float width = 9.625
			float height = 29.6875
			float xOffset = 0
			float yOffset = 21.6875
			float xAdvance = 9.625
			float scale = 1
		[1]
		TMP_Glyph data
			int id = 33
			float x = 91
			float y = 46
			float width = 4.125
			float height = 21.625
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 7.9375
			float scale = 1
		[2]
		TMP_Glyph data
			int id = 34
			float x = 121
			float y = 210
			float width = 8.25
			float height = 6.8125
			float xOffset = 1.875
			float yOffset = 21.5
			float xAdvance = 12.125
			float scale = 1
		[3]
		TMP_Glyph data
			int id = 35
			float x = 29
			float y = 145
			float width = 22.75
			float height = 22.125
			float xOffset = 1.875
			float yOffset = 21.9375
			float xAdvance = 26.5625
			float scale = 1
		[4]
		TMP_Glyph data
			int id = 36
			float x = 27
			float y = 100
			float width = 14.8125
			float height = 23.75
			float xOffset = 1.875
			float yOffset = 22.9375
			float xAdvance = 18.6875
			float scale = 1
		[5]
		TMP_Glyph data
			int id = 37
			float x = 6
			float y = 18
			float width = 18.875
			float height = 23.75
			float xOffset = 0.75
			float yOffset = 22.6875
			float xAdvance = 21.625
			float scale = 1
		[6]
		TMP_Glyph data
			int id = 38
			float x = 50
			float y = 67
			float width = 15.6875
			float height = 21.75
			float xOffset = 1.875
			float yOffset = 21.75
			float xAdvance = 19.5
			float scale = 1
		[7]
		TMP_Glyph data
			int id = 39
			float x = 132
			float y = 156
			float width = 3.875
			float height = 7
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 7.75
			float scale = 1
		[8]
		TMP_Glyph data
			int id = 40
			float x = 6
			float y = 218
			float width = 8.25
			float height = 31.625
			float xOffset = 1.875
			float yOffset = 22.25
			float xAdvance = 12.125
			float scale = 1
		[9]
		TMP_Glyph data
			int id = 41
			float x = 6
			float y = 176
			float width = 8.3125
			float height = 30.375
			float xOffset = 1.875
			float yOffset = 22.1875
			float xAdvance = 12.125
			float scale = 1
		[10]
		TMP_Glyph data
			int id = 42
			float x = 128
			float y = 243
			float width = 7.875
			float height = 6.625
			float xOffset = 1.875
			float yOffset = 21.625
			float xAdvance = 11.6875
			float scale = 1
		[11]
		TMP_Glyph data
			int id = 43
			float x = 107
			float y = 53
			float width = 16.125
			float height = 14.375
			float xOffset = 1.875
			float yOffset = 18
			float xAdvance = 19.9375
			float scale = 1
		[12]
		TMP_Glyph data
			int id = 44
			float x = 131
			float y = 79
			float width = 5.1875
			float height = 5.375
			float xOffset = 1.125
			float yOffset = 3.1875
			float xAdvance = 8.3125
			float scale = 1
		[13]
		TMP_Glyph data
			int id = 45
			float x = 105
			float y = 122
			float width = 12
			float height = 2.8125
			float xOffset = 1.875
			float yOffset = 10.875
			float xAdvance = 15.8125
			float scale = 1
		[14]
		TMP_Glyph data
			int id = 46
			float x = 128
			float y = 122
			float width = 5.3125
			float height = 2.8125
			float xOffset = 1.25
			float yOffset = 2.875
			float xAdvance = 7.9375
			float scale = 1
		[15]
		TMP_Glyph data
			int id = 47
			float x = 21
			float y = 59
			float width = 17.875
			float height = 23.75
			float xOffset = 0.625
			float yOffset = 22.625
			float xAdvance = 20.6875
			float scale = 1
		[16]
		TMP_Glyph data
			int id = 48
			float x = 89
			float y = 162
			float width = 13.8125
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 17.6875
			float scale = 1
		[17]
		TMP_Glyph data
			int id = 49
			float x = 109
			float y = 228
			float width = 7.25
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 11.125
			float scale = 1
		[18]
		TMP_Glyph data
			int id = 50
			float x = 65
			float y = 34
			float width = 14.8125
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 18.6875
			float scale = 1
		[19]
		TMP_Glyph data
			int id = 51
			float x = 97
			float y = 195
			float width = 13
			float height = 21.75
			float xOffset = 1.875
			float yOffset = 21.75
			float xAdvance = 16.875
			float scale = 1
		[20]
		TMP_Glyph data
			int id = 52
			float x = 53
			float y = 112
			float width = 14.75
			float height = 21.75
			float xOffset = 1.875
			float yOffset = 21.75
			float xAdvance = 18.5625
			float scale = 1
		[21]
		TMP_Glyph data
			int id = 53
			float x = 63
			float y = 153
			float width = 14.8125
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 18.6875
			float scale = 1
		[22]
		TMP_Glyph data
			int id = 54
			float x = 71
			float y = 195
			float width = 14.875
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 19.0625
			float scale = 1
		[23]
		TMP_Glyph data
			int id = 55
			float x = 83
			float y = 228
			float width = 14.6875
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 19.5
			float scale = 1
		[24]
		TMP_Glyph data
			int id = 56
			float x = 77
			float y = 79
			float width = 14.6875
			float height = 21.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 18.5625
			float scale = 1
		[25]
		TMP_Glyph data
			int id = 57
			float x = 79
			float y = 120
			float width = 14.625
			float height = 21.75
			float xOffset = 1.875
			float yOffset = 21.75
			float xAdvance = 18.5
			float scale = 1
		[26]
		TMP_Glyph data
			int id = 58
			float x = 130
			float y = 96
			float width = 4.75
			float height = 14.375
			float xOffset = 1.25
			float yOffset = 14.4375
			float xAdvance = 7.9375
			float scale = 1
		[27]
		TMP_Glyph data
			int id = 59
			float x = 83
			float y = 6
			float width = 6.8125
			float height = 16.0625
			float xOffset = 1.9375
			float yOffset = 13.875
			float xAdvance = 8.3125
			float scale = 1
		[28]
		TMP_Glyph data
			int id = 60
			float x = 101
			float y = 19
			float width = 17.3125
			float height = 15.3125
			float xOffset = 1.875
			float yOffset = 18.6875
			float xAdvance = 20.1875
			float scale = 1
		[29]
		TMP_Glyph data
			int id = 61
			float x = 114
			float y = 174
			float width = 14.5
			float height = 9.8125
			float xOffset = 1.875
			float yOffset = 15.5625
			float xAdvance = 18.3125
			float scale = 1
		[30]
		TMP_Glyph data
			int id = 62
			float x = 105
			float y = 136
			float width = 16
			float height = 14.875
			float xOffset = 1.9375
			float yOffset = 17.6875
			float xAdvance = 19.8125
			float scale = 1
		[31]
		TMP_Glyph data
			int id = 63
			float x = 36
			float y = 26
			float width = 17.625
			float height = 21.8125
			float xOffset = -0.1875
			float yOffset = 21.75
			float xAdvance = 19.3125
			float scale = 1
		[32]
		TMP_Glyph data
			int id = 64
			float x = 49
			float y = 228
			float width = 22.3125
			float height = 21.5
			float xOffset = 1.875
			float yOffset = 16.75
			float xAdvance = 26.125
			float scale = 1
		[33]
		TMP_Glyph data
			int id = 91
			float x = 26
			float y = 179
			float width = 9.75
			float height = 29.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 13.625
			float scale = 1
		[34]
		TMP_Glyph data
			int id = 92
			float x = 47
			float y = 186
			float width = 12.6875
			float height = 22.3125
			float xOffset = 1.75
			float yOffset = 22.0625
			float xAdvance = 16.1875
			float scale = 1
		[35]
		TMP_Glyph data
			int id = 93
			float x = 6
			float y = 94
			float width = 9.75
			float height = 29.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 13.625
			float scale = 1
		[36]
		TMP_Glyph data
			int id = 94
			float x = 103
			float y = 93
			float width = 15.25
			float height = 15.4375
			float xOffset = 1.5625
			float yOffset = 21.75
			float xAdvance = 18.9375
			float scale = 1
		[37]
		TMP_Glyph data
			int id = 95
			float x = 103
			float y = 79
			float width = 16.25
			float height = 2.8125
			float xOffset = 0
			float yOffset = -5.125
			float xAdvance = 16.25
			float scale = 1
		[38]
		TMP_Glyph data
			int id = 96
			float x = 36
			float y = 7
			float width = 8
			float height = 7.875
			float xOffset = 1.1875
			float yOffset = 22.4375
			float xAdvance = 11.3125
			float scale = 1
		[39]
		TMP_Glyph data
			int id = 123
			float x = 6
			float y = 135
			float width = 12
			float height = 29.75
			float xOffset = 1.75
			float yOffset = 21.6875
			float xAdvance = 14.875
			float scale = 1
		[40]
		TMP_Glyph data
			int id = 124
			float x = 6
			float y = 53
			float width = 4
			float height = 29.6875
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 7.875
			float scale = 1
		[41]
		TMP_Glyph data
			int id = 125
			float x = 26
			float y = 220
			float width = 11.1875
			float height = 29.625
			float xOffset = 1.875
			float yOffset = 21.6875
			float xAdvance = 14.875
			float scale = 1
		[42]
		TMP_Glyph data
			int id = 126
			float x = 55
			float y = 8
			float width = 16.1875
			float height = 6.5
			float xOffset = 1.875
			float yOffset = 13.1875
			float xAdvance = 20.0625
			float scale = 1
		[43]
		TMP_Glyph data
			int id = 160
			float x = 6
			float y = 261
			float width = 0
			float height = 0
			float xOffset = 0
			float yOffset = 0
			float xAdvance = 0
			float scale = 1
KerningTable m_kerningInfo
	KerningPair kerningPairs
		Array Array
		int size = 0
KerningPair m_kerningPair
	int AscII_Left = 0
	int AscII_Right = 0
	float XadvanceOffset = 0
vector fallbackFontAssets
	Array Array
	int size = 0
FontCreationSetting fontCreationSettings
	string fontSourcePath = ""
	int fontSizingMode = 0
	int fontSize = 0
	int fontPadding = 0
	int fontPackingMode = 0
	int fontAtlasWidth = 0
	int fontAtlasHeight = 0
	int fontCharacterSet = 0
	int fontStyle = 0
	float fontStlyeModifier = 0
	int fontRenderMode = 0
	UInt8 fontKerning = 0
TMP_FontWeights fontWeights
	Array Array
	int size = 10
		[0]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[1]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[2]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[3]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[4]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[5]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[6]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[7]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[8]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
		[9]
		TMP_FontWeights data
			PPtr<$TMP_FontAsset> regularTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
			PPtr<$TMP_FontAsset> italicTypeface
				int m_FileID = 0
				SInt64 m_PathID = 0
float normalStyle = 0
float normalSpacingOffset = 0
float boldStyle = 0.75
float boldSpacing = 7
UInt8 italicStyle = 35
UInt8 tabSize = 10
