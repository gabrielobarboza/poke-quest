PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 1088006837878153380
string m_Name = "EnemyPack_grass1_001"
EnemySet m_normalPrimaryEnemySets
	Array Array
	int size = 2
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -7839319388312639366
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4593165393008925920
						int m_lotteryRate = 100
EnemySet m_normalSecondaryEnemySets
	Array Array
	int size = 2
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -4075980691800135049
						int m_lotteryRate = 60
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 2
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -6947315398813305206
						int m_lotteryRate = 95
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4187397011823263866
						int m_lotteryRate = 5
EnemySet m_normalTertiaryEnemySets
	Array Array
	int size = 2
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -4075980691800135049
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 2
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -6947315398813305206
						int m_lotteryRate = 95
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4187397011823263866
						int m_lotteryRate = 5
EnemySet m_normalQuarternaryEnemySets
	Array Array
	int size = 2
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -4075980691800135049
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 2
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -6947315398813305206
						int m_lotteryRate = 95
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4187397011823263866
						int m_lotteryRate = 5
EnemySet m_bossPrimaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 2
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 2718829844357237322
				int m_lotteryRate = 100
			[1]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 454013806193275644
				int m_lotteryRate = 100
EnemySet m_bossSecondaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 0
EnemySet m_bossTertiaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
EnemySet m_bossQuarternaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
RegistData m_registDatas
	Array Array
	int size = 12
		[0]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 1
			int m_spawnMode = 1
		[1]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 0
			int m_spawnMode = 1
		[2]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 0
			int m_spawnMode = 1
		[3]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 3
			int m_setIndex = 0
			int m_spawnMode = 1
		[4]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 1
			int m_spawnMode = 1
		[5]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 0
			int m_spawnMode = 1
		[6]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 0
			int m_spawnMode = 1
		[7]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 3
			int m_setIndex = 0
			int m_spawnMode = 1
		[8]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 1
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
		[9]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 1
			int m_spawnMode = 0
		[10]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 1
			int m_spawnMode = 0
		[11]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 3
			int m_setIndex = 1
			int m_spawnMode = 0
