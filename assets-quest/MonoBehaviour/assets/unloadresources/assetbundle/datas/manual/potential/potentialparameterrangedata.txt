PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = -5453272390430372218
string m_Name = "PotentialParameterRangeData"
Set m_hpValue
	float m_min = 0
	float m_max = 9999
Set m_attackValue
	float m_min = 0
	float m_max = 9999
Set m_hpPercent
	float m_min = 0
	float m_max = 0
Set m_attackPercent
	float m_min = 0
	float m_max = 0
Set m_criticalChancePercent
	float m_min = 0
	float m_max = 1
Set m_criticalDamagePercent
	float m_min = 0
	float m_max = 1
Set m_criticalChanceBlockPercent
	float m_min = -1
	float m_max = 0
Set m_chargeSecond
	float m_min = 0
	float m_max = 0
Set m_chargeSecondPercent
	float m_min = -0.9
	float m_max = 2
Set m_chargeSecondPercentNormalSkill
	float m_min = -0.9
	float m_max = 0
Set m_distance
	float m_min = 0
	float m_max = 3
Set m_distancePercent
	float m_min = 0
	float m_max = 1
Set m_skillObjectExtendNum
	float m_min = 0
	float m_max = 3
Set m_skillLoopNum
	float m_min = 0
	float m_max = 3
Set m_skillAttackValue
	float m_min = 0
	float m_max = 0
Set m_skillAttackPercent
	float m_min = -0.5
	float m_max = 9
Set m_nwayNum
	float m_min = 0
	float m_max = 3
Set m_regenerationValue
	float m_min = 0
	float m_max = 0
Set m_regenerationPercent
	float m_min = 0
	float m_max = 3
Set m_lifeGainOnHitValue
	float m_min = 0
	float m_max = 0
Set m_lifeGainOnHitPercent
	float m_min = 0
	float m_max = 0.1
Set m_lifeGainOnKillValue
	float m_min = 0
	float m_max = 0
Set m_lifeGainOnKillPercent
	float m_min = 0
	float m_max = 0.1
Set m_moveSpeedPercent
	float m_min = -1
	float m_max = 2
Set m_buffProbPercent
	float m_min = -1
	float m_max = 1
Set m_buffTimePercent
	float m_min = -0.9
	float m_max = 1
Set m_debuffProbPercent
	float m_min = -1
	float m_max = 1
Set m_debuffTimePercent
	float m_min = -1
	float m_max = 1
Set m_homingValue
	float m_min = 0
	float m_max = 3
Set m_revivalTimePercent
	float m_min = -0.5
	float m_max = 0
Set m_revivalRecoverPercent
	float m_min = 0
	float m_max = 0.5
Set m_modelScalePercent
	float m_min = -0.5
	float m_max = 0.5
Set m_knockBackDistancePercent
	float m_min = -1
	float m_max = 0
Set m_knockBackOtherDistancePercent
	float m_min = 0
	float m_max = 2
Set m_healPercent
	float m_min = 0
	float m_max = 2
Set m_buffSharePercent
	float m_min = 0
	float m_max = 1
Set m_waveRecoverPercent
	float m_min = 0
	float m_max = 1
Set m_conditionPercent
	float m_min = -1
	float m_max = 0
PokeTypeValues m_pokeTypeValues
	Set m_attakValue
		float m_min = 0
		float m_max = 0
	Set m_attackPercent
		float m_min = 0
		float m_max = 0
	Set m_hpValue
		float m_min = 0
		float m_max = 0
	Set m_hpPercent
		float m_min = 0
		float m_max = 0
	Set m_chargeSecond
		float m_min = 0
		float m_max = 0
	Set m_chargeSecondPercent
		float m_min = 0
		float m_max = 0
	Set m_chargeSecondPercentSkillType
		float m_min = -0.5
		float m_max = 0
	Set m_daamgeBlockPercentSkillType
		float m_min = -0.9
		float m_max = 0
	Set m_skillAttackPercentSkillType
		float m_min = 0
		float m_max = 1
