PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = -3569404403969806053
string m_Name = "ConditionDataSet"
ConditionData m_datas
	Array Array
	int size = 25
		[0]
		ConditionData data
			int m_id = 0
			int m_type = 0
			float m_Value_A = 0
			float m_Value_B = 0
			float m_time = 0
			float m_ratio = 1
		[1]
		ConditionData data
			int m_id = 1
			int m_type = 1
			float m_Value_A = 0.4
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.08
		[2]
		ConditionData data
			int m_id = 2
			int m_type = 3
			float m_Value_A = 0.4
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.08
		[3]
		ConditionData data
			int m_id = 3
			int m_type = 5
			float m_Value_A = 0.3
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.08
		[4]
		ConditionData data
			int m_id = 4
			int m_type = 7
			float m_Value_A = 0.5
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.08
		[5]
		ConditionData data
			int m_id = 5
			int m_type = 1
			float m_Value_A = 0.6
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.08
		[6]
		ConditionData data
			int m_id = 6
			int m_type = 3
			float m_Value_A = 0.6
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.08
		[7]
		ConditionData data
			int m_id = 7
			int m_type = 5
			float m_Value_A = 0.45
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.08
		[8]
		ConditionData data
			int m_id = 8
			int m_type = 7
			float m_Value_A = 0.75
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.08
		[9]
		ConditionData data
			int m_id = 51
			int m_type = 2
			float m_Value_A = 0.4
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.12
		[10]
		ConditionData data
			int m_id = 52
			int m_type = 4
			float m_Value_A = 0.4
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.12
		[11]
		ConditionData data
			int m_id = 53
			int m_type = 6
			float m_Value_A = 0.3
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.12
		[12]
		ConditionData data
			int m_id = 54
			int m_type = 8
			float m_Value_A = 0.5
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.12
		[13]
		ConditionData data
			int m_id = 55
			int m_type = 2
			float m_Value_A = 0.6
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.12
		[14]
		ConditionData data
			int m_id = 56
			int m_type = 4
			float m_Value_A = 0.6
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.12
		[15]
		ConditionData data
			int m_id = 57
			int m_type = 6
			float m_Value_A = 0.45
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.12
		[16]
		ConditionData data
			int m_id = 58
			int m_type = 8
			float m_Value_A = 0.75
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.12
		[17]
		ConditionData data
			int m_id = 101
			int m_type = 9
			float m_Value_A = 0.5
			float m_Value_B = 0
			float m_time = 20
			float m_ratio = 0.1
		[18]
		ConditionData data
			int m_id = 102
			int m_type = 10
			float m_Value_A = 0.8
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.1
		[19]
		ConditionData data
			int m_id = 103
			int m_type = 11
			float m_Value_A = 0
			float m_Value_B = 0
			float m_time = 10
			float m_ratio = 0.08
		[20]
		ConditionData data
			int m_id = 104
			int m_type = 12
			float m_Value_A = 0
			float m_Value_B = 0
			float m_time = 10
			float m_ratio = 0.08
		[21]
		ConditionData data
			int m_id = 105
			int m_type = 13
			float m_Value_A = 0.5
			float m_Value_B = 0.8
			float m_time = 15
			float m_ratio = 0.08
		[22]
		ConditionData data
			int m_id = 106
			int m_type = 14
			float m_Value_A = 1
			float m_Value_B = 0
			float m_time = 15
			float m_ratio = 0.1
		[23]
		ConditionData data
			int m_id = 107
			int m_type = 15
			float m_Value_A = 0
			float m_Value_B = 0
			float m_time = 18
			float m_ratio = 0.1
		[24]
		ConditionData data
			int m_id = 108
			int m_type = 16
			float m_Value_A = 0
			float m_Value_B = 0
			float m_time = 10
			float m_ratio = 0.08
