PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 4819576707525267693
string m_Name = "StageItemDropWeightDataSet"
StageItemDropWeightData m_datas
	Array Array
	int size = 14
		[0]
		StageItemDropWeightData data
			float m_redCommon = 4
			float m_redUnCommon = 4
			float m_blueCommon = 3
			float m_blueUnCommon = 3
			float m_yellowCommon = 1
			float m_yellowUnCommon = 1
			float m_greyCommon = 1
			float m_greyUnCommon = 1
			float m_rare = 1
			float m_legend = 0
		[1]
		StageItemDropWeightData data
			float m_redCommon = 1
			float m_redUnCommon = 1
			float m_blueCommon = 1
			float m_blueUnCommon = 1
			float m_yellowCommon = 4
			float m_yellowUnCommon = 4
			float m_greyCommon = 3
			float m_greyUnCommon = 3
			float m_rare = 1
			float m_legend = 0
		[2]
		StageItemDropWeightData data
			float m_redCommon = 1
			float m_redUnCommon = 1
			float m_blueCommon = 4
			float m_blueUnCommon = 4
			float m_yellowCommon = 3
			float m_yellowUnCommon = 3
			float m_greyCommon = 1
			float m_greyUnCommon = 1
			float m_rare = 1
			float m_legend = 0
		[3]
		StageItemDropWeightData data
			float m_redCommon = 3
			float m_redUnCommon = 3
			float m_blueCommon = 1
			float m_blueUnCommon = 1
			float m_yellowCommon = 1
			float m_yellowUnCommon = 1
			float m_greyCommon = 4
			float m_greyUnCommon = 4
			float m_rare = 1
			float m_legend = 0
		[4]
		StageItemDropWeightData data
			float m_redCommon = 1
			float m_redUnCommon = 1
			float m_blueCommon = 4
			float m_blueUnCommon = 4
			float m_yellowCommon = 1
			float m_yellowUnCommon = 1
			float m_greyCommon = 3
			float m_greyUnCommon = 3
			float m_rare = 1
			float m_legend = 0
		[5]
		StageItemDropWeightData data
			float m_redCommon = 3
			float m_redUnCommon = 3
			float m_blueCommon = 1
			float m_blueUnCommon = 1
			float m_yellowCommon = 4
			float m_yellowUnCommon = 4
			float m_greyCommon = 1
			float m_greyUnCommon = 1
			float m_rare = 1
			float m_legend = 0
		[6]
		StageItemDropWeightData data
			float m_redCommon = 1
			float m_redUnCommon = 1
			float m_blueCommon = 2
			float m_blueUnCommon = 2
			float m_yellowCommon = 4
			float m_yellowUnCommon = 4
			float m_greyCommon = 2
			float m_greyUnCommon = 2
			float m_rare = 1
			float m_legend = 0
		[7]
		StageItemDropWeightData data
			float m_redCommon = 2
			float m_redUnCommon = 2
			float m_blueCommon = 4
			float m_blueUnCommon = 4
			float m_yellowCommon = 2
			float m_yellowUnCommon = 2
			float m_greyCommon = 1
			float m_greyUnCommon = 1
			float m_rare = 1
			float m_legend = 0
		[8]
		StageItemDropWeightData data
			float m_redCommon = 4
			float m_redUnCommon = 4
			float m_blueCommon = 1
			float m_blueUnCommon = 1
			float m_yellowCommon = 2
			float m_yellowUnCommon = 2
			float m_greyCommon = 2
			float m_greyUnCommon = 2
			float m_rare = 1
			float m_legend = 0
		[9]
		StageItemDropWeightData data
			float m_redCommon = 2
			float m_redUnCommon = 2
			float m_blueCommon = 2
			float m_blueUnCommon = 2
			float m_yellowCommon = 1
			float m_yellowUnCommon = 1
			float m_greyCommon = 4
			float m_greyUnCommon = 4
			float m_rare = 1
			float m_legend = 0
		[10]
		StageItemDropWeightData data
			float m_redCommon = 2
			float m_redUnCommon = 2
			float m_blueCommon = 2
			float m_blueUnCommon = 2
			float m_yellowCommon = 2
			float m_yellowUnCommon = 2
			float m_greyCommon = 2
			float m_greyUnCommon = 2
			float m_rare = 2
			float m_legend = 0
		[11]
		StageItemDropWeightData data
			float m_redCommon = 1
			float m_redUnCommon = 1
			float m_blueCommon = 1
			float m_blueUnCommon = 1
			float m_yellowCommon = 1
			float m_yellowUnCommon = 1
			float m_greyCommon = 1
			float m_greyUnCommon = 1
			float m_rare = 1
			float m_legend = 0
		[12]
		StageItemDropWeightData data
			float m_redCommon = 2
			float m_redUnCommon = 2
			float m_blueCommon = 2
			float m_blueUnCommon = 2
			float m_yellowCommon = 2
			float m_yellowUnCommon = 2
			float m_greyCommon = 2
			float m_greyUnCommon = 2
			float m_rare = 2
			float m_legend = 2
		[13]
		StageItemDropWeightData data
			float m_redCommon = 100
			float m_redUnCommon = 0
			float m_blueCommon = 100
			float m_blueUnCommon = 0
			float m_yellowCommon = 0
			float m_yellowUnCommon = 0
			float m_greyCommon = 0
			float m_greyUnCommon = 0
			float m_rare = 0
			float m_legend = 0
