PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = -740577217861329660
string m_Name = "CookVolumeDataSet"
CookVolumeData m_datas
	Array Array
	int size = 4
		[0]
		CookVolumeData data
			SInt16 m_itemNum = 3
			int m_pokeLevelMin = 1
			int m_pokeLevelMax = 15
			string m_uiPath = "Item_Pot_1"
			string m_modelPath = "BC_cauldron01"
			float m_cookTime = 0
		[1]
		CookVolumeData data
			SInt16 m_itemNum = 10
			int m_pokeLevelMin = 16
			int m_pokeLevelMax = 30
			string m_uiPath = "Item_Pot_2"
			string m_modelPath = "BC_cauldron02"
			float m_cookTime = 0
		[2]
		CookVolumeData data
			SInt16 m_itemNum = 15
			int m_pokeLevelMin = 31
			int m_pokeLevelMax = 70
			string m_uiPath = "Item_Pot_3"
			string m_modelPath = "BC_cauldron03"
			float m_cookTime = 1
		[3]
		CookVolumeData data
			SInt16 m_itemNum = 20
			int m_pokeLevelMin = 71
			int m_pokeLevelMax = 100
			string m_uiPath = "Item_Pot_4"
			string m_modelPath = "BC_cauldron04"
			float m_cookTime = 2
