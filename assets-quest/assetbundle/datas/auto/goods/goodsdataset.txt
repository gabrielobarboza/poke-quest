PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 6445673810394149375
string m_Name = "GoodsDataSet"
GoodsData m_datas
	Array Array
	int size = 39
		[0]
		GoodsData data
			int m_id = 100
			int m_mstxtID = 0
			int m_category = 0
			string m_modelPath = "G_makeover_arch_1"
			string m_iconPath = "G_tex_makeover_arch_1"
			int m_effectID = 2
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1
					[1]
					float data = 0
			int m_direction = 2
		[1]
		GoodsData data
			int m_id = 101
			int m_mstxtID = 1
			int m_category = 0
			string m_modelPath = "G_makeover_arch_2"
			string m_iconPath = "G_tex_makeover_arch_2"
			int m_effectID = 2
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1
					[1]
					float data = 0
			int m_direction = 2
		[2]
		GoodsData data
			int m_id = 102
			int m_mstxtID = 2
			int m_category = 0
			string m_modelPath = "G_makeover_arch_3"
			string m_iconPath = "G_tex_makeover_arch_3"
			int m_effectID = 2
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1
					[1]
					float data = 0
			int m_direction = 2
		[3]
		GoodsData data
			int m_id = 103
			int m_mstxtID = 3
			int m_category = 0
			string m_modelPath = "G_makeover_arch_4"
			string m_iconPath = "G_tex_makeover_arch_4"
			int m_effectID = 2
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1
					[1]
					float data = 0
			int m_direction = 2
		[4]
		GoodsData data
			int m_id = 200
			int m_mstxtID = 4
			int m_category = 1
			string m_modelPath = "G_makeover_L_1"
			string m_iconPath = "G_tex_makeover_L_1"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 1
			int m_direction = 2
		[5]
		GoodsData data
			int m_id = 201
			int m_mstxtID = 5
			int m_category = 1
			string m_modelPath = "G_makeover_L_2"
			string m_iconPath = "G_tex_makeover_L_2"
			int m_effectID = 3
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 2
		[6]
		GoodsData data
			int m_id = 202
			int m_mstxtID = 6
			int m_category = 1
			string m_modelPath = "G_makeover_L_3"
			string m_iconPath = "G_tex_makeover_L_3"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 3
			int m_direction = 2
		[7]
		GoodsData data
			int m_id = 203
			int m_mstxtID = 7
			int m_category = 1
			string m_modelPath = "G_makeover_L_4"
			string m_iconPath = "G_tex_makeover_L_4"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 4
			int m_direction = 2
		[8]
		GoodsData data
			int m_id = 204
			int m_mstxtID = 8
			int m_category = 1
			string m_modelPath = "G_makeover_L_5"
			string m_iconPath = "G_tex_makeover_L_5"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 2
			int m_direction = 2
		[9]
		GoodsData data
			int m_id = 205
			int m_mstxtID = 9
			int m_category = 1
			string m_modelPath = "G_makeover_L_6"
			string m_iconPath = "G_tex_makeover_L_6"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 4
			int m_direction = 2
		[10]
		GoodsData data
			int m_id = 206
			int m_mstxtID = 10
			int m_category = 1
			string m_modelPath = "G_makeover_L_7"
			string m_iconPath = "G_tex_makeover_L_7"
			int m_effectID = 1
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 2
		[11]
		GoodsData data
			int m_id = 207
			int m_mstxtID = 11
			int m_category = 1
			string m_modelPath = "G_makeover_L_8"
			string m_iconPath = "G_tex_makeover_L_8"
			int m_effectID = 8
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 2
		[12]
		GoodsData data
			int m_id = 300
			int m_mstxtID = 12
			int m_category = 2
			string m_modelPath = "G_makeover_S_1"
			string m_iconPath = "G_tex_makeover_S_1"
			int m_effectID = 3
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 0
			int m_direction = 2
		[13]
		GoodsData data
			int m_id = 301
			int m_mstxtID = 13
			int m_category = 2
			string m_modelPath = "G_makeover_S_2"
			string m_iconPath = "G_tex_makeover_S_2"
			int m_effectID = 8
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 0
			int m_direction = 0
		[14]
		GoodsData data
			int m_id = 302
			int m_mstxtID = 14
			int m_category = 2
			string m_modelPath = "G_makeover_S_3"
			string m_iconPath = "G_tex_makeover_S_3"
			int m_effectID = 5
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 3
		[15]
		GoodsData data
			int m_id = 303
			int m_mstxtID = 15
			int m_category = 2
			string m_modelPath = "G_makeover_S_4"
			string m_iconPath = "G_tex_makeover_S_4"
			int m_effectID = 7
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 1
		[16]
		GoodsData data
			int m_id = 304
			int m_mstxtID = 16
			int m_category = 2
			string m_modelPath = "G_makeover_S_5"
			string m_iconPath = "G_tex_makeover_S_5"
			int m_effectID = 6
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 2
		[17]
		GoodsData data
			int m_id = 305
			int m_mstxtID = 17
			int m_category = 2
			string m_modelPath = "G_makeover_S_6"
			string m_iconPath = "G_tex_makeover_S_6"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 2
					[1]
					float data = 0
			int m_direction = 0
		[18]
		GoodsData data
			int m_id = 306
			int m_mstxtID = 18
			int m_category = 2
			string m_modelPath = "G_makeover_S_7"
			string m_iconPath = "G_tex_makeover_S_7"
			int m_effectID = 5
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 0
			int m_direction = 3
		[19]
		GoodsData data
			int m_id = 307
			int m_mstxtID = 19
			int m_category = 2
			string m_modelPath = "G_makeover_S_8"
			string m_iconPath = "G_tex_makeover_S_8"
			int m_effectID = 6
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 0
			int m_direction = 1
		[20]
		GoodsData data
			int m_id = 308
			int m_mstxtID = 20
			int m_category = 2
			string m_modelPath = "G_makeover_S_9"
			string m_iconPath = "G_tex_makeover_S_9"
			int m_effectID = 7
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 0
			int m_direction = 2
		[21]
		GoodsData data
			int m_id = 309
			int m_mstxtID = 21
			int m_category = 2
			string m_modelPath = "G_makeover_S_10"
			string m_iconPath = "G_tex_makeover_S_10"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 1
			int m_direction = 0
		[22]
		GoodsData data
			int m_id = 310
			int m_mstxtID = 22
			int m_category = 2
			string m_modelPath = "G_makeover_S_11"
			string m_iconPath = "G_tex_makeover_S_11"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 2
			int m_direction = 3
		[23]
		GoodsData data
			int m_id = 311
			int m_mstxtID = 23
			int m_category = 2
			string m_modelPath = "G_makeover_S_12"
			string m_iconPath = "G_tex_makeover_S_12"
			int m_effectID = 4
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 3
			int m_direction = 1
		[24]
		GoodsData data
			int m_id = 312
			int m_mstxtID = 24
			int m_category = 2
			string m_modelPath = "G_statue_koratta"
			string m_iconPath = "G_tex_statue_koratta"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 5
			int m_direction = 2
		[25]
		GoodsData data
			int m_id = 313
			int m_mstxtID = 25
			int m_category = 2
			string m_modelPath = "G_statue_nyoromo"
			string m_iconPath = "G_tex_statue_nyoromo"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 10
			int m_direction = 0
		[26]
		GoodsData data
			int m_id = 314
			int m_mstxtID = 26
			int m_category = 2
			string m_modelPath = "G_statue_dodo"
			string m_iconPath = "G_tex_statue_dodo"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 15
			int m_direction = 3
		[27]
		GoodsData data
			int m_id = 315
			int m_mstxtID = 27
			int m_category = 2
			string m_modelPath = "G_statue_karakara"
			string m_iconPath = "G_tex_statue_karakara"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 20
			int m_direction = 1
		[28]
		GoodsData data
			int m_id = 316
			int m_mstxtID = 28
			int m_category = 2
			string m_modelPath = "G_statue_pigeon"
			string m_iconPath = "G_tex_statue_pigeon"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 25
			int m_direction = 2
		[29]
		GoodsData data
			int m_id = 317
			int m_mstxtID = 29
			int m_category = 2
			string m_modelPath = "G_statue_monjara"
			string m_iconPath = "G_tex_statue_monjara"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 30
			int m_direction = 0
		[30]
		GoodsData data
			int m_id = 318
			int m_mstxtID = 30
			int m_category = 2
			string m_modelPath = "G_statue_golbat"
			string m_iconPath = "G_tex_statue_golbat"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 35
			int m_direction = 3
		[31]
		GoodsData data
			int m_id = 319
			int m_mstxtID = 31
			int m_category = 2
			string m_modelPath = "G_statue_gallop"
			string m_iconPath = "G_tex_statue_gallop"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 40
			int m_direction = 1
		[32]
		GoodsData data
			int m_id = 320
			int m_mstxtID = 32
			int m_category = 2
			string m_modelPath = "G_statue_arbok"
			string m_iconPath = "G_tex_statue_arbok"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 50
			int m_direction = 2
		[33]
		GoodsData data
			int m_id = 321
			int m_mstxtID = 33
			int m_category = 2
			string m_modelPath = "G_statue_gyarados"
			string m_iconPath = "G_tex_statue_gyarados"
			int m_effectID = 9
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 1.5
					[1]
					float data = 60
			int m_direction = 3
		[34]
		GoodsData data
			int m_id = 400
			int m_mstxtID = 34
			int m_category = 2
			string m_modelPath = "G_ball_monster"
			string m_iconPath = "G_tex_ball_monster"
			int m_effectID = 10
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 20
					[1]
					float data = 0
			int m_direction = 1
		[35]
		GoodsData data
			int m_id = 401
			int m_mstxtID = 35
			int m_category = 2
			string m_modelPath = "G_ball_super"
			string m_iconPath = "G_tex_ball_super"
			int m_effectID = 10
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 30
					[1]
					float data = 0
			int m_direction = 2
		[36]
		GoodsData data
			int m_id = 402
			int m_mstxtID = 36
			int m_category = 2
			string m_modelPath = "G_ball_hyper"
			string m_iconPath = "G_tex_ball_hyper"
			int m_effectID = 10
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 40
					[1]
					float data = 0
			int m_direction = 0
		[37]
		GoodsData data
			int m_id = 403
			int m_mstxtID = 37
			int m_category = 2
			string m_modelPath = "G_ball_premier"
			string m_iconPath = "G_tex_ball_premier"
			int m_effectID = 10
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 10
					[1]
					float data = 0
			int m_direction = 3
		[38]
		GoodsData data
			int m_id = 404
			int m_mstxtID = 38
			int m_category = 2
			string m_modelPath = "G_ball_master"
			string m_iconPath = "G_tex_ball_master"
			int m_effectID = 10
			vector m_effectValue
				Array Array
				int size = 2
					[0]
					float data = 50
					[1]
					float data = 0
			int m_direction = 1
