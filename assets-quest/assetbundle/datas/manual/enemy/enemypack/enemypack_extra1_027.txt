PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 1088006837878153380
string m_Name = "EnemyPack_extra1_027"
EnemySet m_normalPrimaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 3
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1039910336044109549
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5047447045311606448
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -923742360795780627
						int m_lotteryRate = 5
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 3
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1681143101257402453
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -7743487354005951679
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 8965113467510723370
						int m_lotteryRate = 5
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3868476691836760880
						int m_lotteryRate = 100
EnemySet m_normalSecondaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 3
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3402473946777186772
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -2630142063288708084
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 37123930777733749
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 3
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -7322221911599256315
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3396178866532385474
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 943206066131614853
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 274032151422545218
						int m_lotteryRate = 100
EnemySet m_normalTertiaryEnemySets
	Array Array
	int size = 1
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 0
						int m_lotteryRate = 0
EnemySet m_normalQuarternaryEnemySets
	Array Array
	int size = 1
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 0
						int m_lotteryRate = 0
EnemySet m_bossPrimaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 4
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = -6180322494496842136
				int m_lotteryRate = 45
			[1]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = -1311144438483354217
				int m_lotteryRate = 100
			[2]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 6142482710672089137
				int m_lotteryRate = 45
			[3]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 8584599535854726021
				int m_lotteryRate = 10
EnemySet m_bossSecondaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 0
EnemySet m_bossTertiaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
EnemySet m_bossQuarternaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
RegistData m_registDatas
	Array Array
	int size = 7
		[0]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 3
			int m_max = 3
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
		[1]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 3
			int m_max = 3
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 0
			int m_spawnMode = 0
		[2]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 1
			int m_spawnMode = 0
		[3]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 1
			int m_spawnMode = 0
		[4]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 2
			int m_spawnMode = 0
		[5]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 2
			int m_spawnMode = 0
		[6]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 1
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
