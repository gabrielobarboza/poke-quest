PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 1088006837878153380
string m_Name = "EnemyPack_extra1_030"
EnemySet m_normalPrimaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 7
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1581712704949775820
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4318527427735170265
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7902397503228418774
						int m_lotteryRate = 100
					[3]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -8295643000570567875
						int m_lotteryRate = 100
					[4]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -6292390081094335900
						int m_lotteryRate = 100
					[5]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -6160004865511967242
						int m_lotteryRate = 100
					[6]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -2736285294384547708
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 13
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -10061438018529530
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1565898603974972559
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4260260173903241593
						int m_lotteryRate = 100
					[3]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1051583718284695574
						int m_lotteryRate = 100
					[4]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -8793483183492506015
						int m_lotteryRate = 100
					[5]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 413848564740952339
						int m_lotteryRate = 100
					[6]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5449164184751648831
						int m_lotteryRate = 100
					[7]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -4568650000487960727
						int m_lotteryRate = 100
					[8]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3804006035662456335
						int m_lotteryRate = 100
					[9]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -3411119607127298668
						int m_lotteryRate = 100
					[10]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 2826781958514392485
						int m_lotteryRate = 100
					[11]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5553808116216708721
						int m_lotteryRate = 100
					[12]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3315274147712941795
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -1437752063174917496
						int m_lotteryRate = 100
EnemySet m_normalSecondaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 8
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 5877227528798073102
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1621211309522935889
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 8758585423110302737
						int m_lotteryRate = 100
					[3]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3868476691836760880
						int m_lotteryRate = 100
					[4]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 274032151422545218
						int m_lotteryRate = 100
					[5]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 3043305060371620770
						int m_lotteryRate = 100
					[6]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 4222737017898275764
						int m_lotteryRate = 100
					[7]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 5739621294262408625
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 12
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -4288628423617323040
						int m_lotteryRate = 100
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1582504592649261093
						int m_lotteryRate = 100
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7082958529510783322
						int m_lotteryRate = 100
					[3]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -6087060015030343615
						int m_lotteryRate = 100
					[4]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -3372923342599913117
						int m_lotteryRate = 100
					[5]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 877468385812812490
						int m_lotteryRate = 100
					[6]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5405826669973615548
						int m_lotteryRate = 100
					[7]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 9073535449475052076
						int m_lotteryRate = 100
					[8]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5148924529152981990
						int m_lotteryRate = 100
					[9]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5839178199969947256
						int m_lotteryRate = 100
					[10]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -2103034421127495900
						int m_lotteryRate = 100
					[11]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5725103445933163622
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -1916068780884945379
						int m_lotteryRate = 100
EnemySet m_normalTertiaryEnemySets
	Array Array
	int size = 1
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 5852828698695588253
						int m_lotteryRate = 100
EnemySet m_normalQuarternaryEnemySets
	Array Array
	int size = 1
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 0
						int m_lotteryRate = 0
EnemySet m_bossPrimaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = -2707177390568436991
				int m_lotteryRate = 100
EnemySet m_bossSecondaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
EnemySet m_bossTertiaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
EnemySet m_bossQuarternaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
RegistData m_registDatas
	Array Array
	int size = 8
		[0]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
		[1]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 0
			int m_spawnMode = 0
		[2]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 1
			int m_spawnMode = 0
		[3]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 1
			int m_spawnMode = 0
		[4]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 2
			int m_spawnMode = 0
		[5]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 2
			int m_spawnMode = 0
		[6]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 0
			int m_spawnMode = 0
		[7]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 1
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
