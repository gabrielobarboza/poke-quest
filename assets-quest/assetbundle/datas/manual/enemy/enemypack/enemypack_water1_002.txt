PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 1088006837878153380
string m_Name = "EnemyPack_water1_002"
EnemySet m_normalPrimaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 3
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 8589311041080981845
						int m_lotteryRate = 140
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7081687248889667414
						int m_lotteryRate = 40
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 5091874815000024294
						int m_lotteryRate = 20
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -2237625167368224752
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7213310169636820303
						int m_lotteryRate = 100
EnemySet m_normalSecondaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 2
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 809139080394890762
						int m_lotteryRate = 290
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 8222632029363129449
						int m_lotteryRate = 10
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 809139080394890762
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7213310169636820303
						int m_lotteryRate = 100
EnemySet m_normalTertiaryEnemySets
	Array Array
	int size = 1
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7213310169636820303
						int m_lotteryRate = 100
EnemySet m_normalQuarternaryEnemySets
	Array Array
	int size = 1
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 0
						int m_lotteryRate = 0
EnemySet m_bossPrimaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 8548727150978710031
				int m_lotteryRate = 100
EnemySet m_bossSecondaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 0
EnemySet m_bossTertiaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
EnemySet m_bossQuarternaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 8548727150978710031
				int m_lotteryRate = 100
RegistData m_registDatas
	Array Array
	int size = 8
		[0]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 1
			int m_spawnMode = 0
		[1]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 1
			int m_spawnMode = 0
		[2]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 3
			int m_max = 3
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
		[3]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 0
			int m_spawnMode = 0
		[4]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 2
			int m_spawnMode = 2
		[5]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 2
			int m_spawnMode = 2
		[6]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 0
			int m_spawnMode = 2
		[7]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 1
			int m_setType = 3
			int m_setIndex = 0
			int m_spawnMode = 2
