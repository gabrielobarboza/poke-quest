PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 982830141640938645
string m_Name = "EnemyData_117_water2"
int m_monsterNo = 117
UInt8 m_formNo = 0
UInt16 m_tokusei = 0
int m_hpBasis = 1200
int m_attackBasis = 250
int m_seikaku = 0
float m_scale = 1.5
int m_normalSkillRangeType = 1
UniqSkiil m_uniqSkills
	Array Array
	int size = 3
		[0]
		UniqSkiil data
			int m_skillID = 51
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 25
					[2]
					int data = -1
		[1]
		UniqSkiil data
			int m_skillID = 140
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 17
					[2]
					int data = 18
		[2]
		UniqSkiil data
			int m_skillID = 137
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 26
vector m_passiveStoneIDs
	Array Array
	int size = 4
		[0]
		int data = 4
		[1]
		int data = 13
		[2]
		int data = 14
		[3]
		int data = 2
CharacterSettingParameterPercent m_characterSettingParameterPercent
	Regeneration m_regeneration
		float m_intervalSecond = 0
		float m_hpPercent = 0
	Eye m_eye
		float m_radius = 1
	NavMeshAgent m_navMeshAgent
		float m_speed = 1
		float m_acceleration = 1
		float m_angularSpeed = 1
int m_AIType = 3
int m_dropType = 2
