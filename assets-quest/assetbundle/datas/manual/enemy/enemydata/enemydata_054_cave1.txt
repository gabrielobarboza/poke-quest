PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 982830141640938645
string m_Name = "EnemyData_054_cave1"
int m_monsterNo = 54
UInt8 m_formNo = 0
UInt16 m_tokusei = 0
int m_hpBasis = 60
int m_attackBasis = 60
int m_seikaku = 15
float m_scale = 1
int m_normalSkillRangeType = 0
UniqSkiil m_uniqSkills
	Array Array
	int size = 3
		[0]
		UniqSkiil data
			int m_skillID = 185
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 15
					[1]
					int data = -1
					[2]
					int data = -1
		[1]
		UniqSkiil data
			int m_skillID = 65535
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = -1
					[1]
					int data = -1
					[2]
					int data = -1
		[2]
		UniqSkiil data
			int m_skillID = 65535
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = -1
					[1]
					int data = -1
					[2]
					int data = -1
vector m_passiveStoneIDs
	Array Array
	int size = 4
		[0]
		int data = 64
		[1]
		int data = -1
		[2]
		int data = -1
		[3]
		int data = -1
CharacterSettingParameterPercent m_characterSettingParameterPercent
	Regeneration m_regeneration
		float m_intervalSecond = 0
		float m_hpPercent = 0
	Eye m_eye
		float m_radius = 1
	NavMeshAgent m_navMeshAgent
		float m_speed = 1
		float m_acceleration = 1
		float m_angularSpeed = 1
int m_AIType = 0
int m_dropType = 0
