PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 982830141640938645
string m_Name = "EnemyData_142_extra1"
int m_monsterNo = 142
UInt8 m_formNo = 0
UInt16 m_tokusei = 0
int m_hpBasis = 1300
int m_attackBasis = 350
int m_seikaku = 0
float m_scale = 2
int m_normalSkillRangeType = 0
UniqSkiil m_uniqSkills
	Array Array
	int size = 3
		[0]
		UniqSkiil data
			int m_skillID = 138
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 39
					[2]
					int data = 18
		[1]
		UniqSkiil data
			int m_skillID = 188
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 45
					[2]
					int data = 17
		[2]
		UniqSkiil data
			int m_skillID = 40
			vector m_enemyStoneID
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 26
					[2]
					int data = -1
vector m_passiveStoneIDs
	Array Array
	int size = 4
		[0]
		int data = 2
		[1]
		int data = 49
		[2]
		int data = -1
		[3]
		int data = -1
CharacterSettingParameterPercent m_characterSettingParameterPercent
	Regeneration m_regeneration
		float m_intervalSecond = 0
		float m_hpPercent = 0
	Eye m_eye
		float m_radius = 1
	NavMeshAgent m_navMeshAgent
		float m_speed = 1
		float m_acceleration = 1
		float m_angularSpeed = 1
int m_AIType = 3
int m_dropType = 2
